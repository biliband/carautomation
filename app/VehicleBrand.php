<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleBrand extends Model
{
    protected $fillable =[
      'vehicle_brand'
    ];

    public function models()
    {
        return $this->hasMany(VehicleModel::class);
    }
}
