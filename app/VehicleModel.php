<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class VehicleModel extends Model
{

    protected $fillable = [
        'model',
        'model_year',
        'vehicle_brand_id',
        'img'
    ];

    public function vehicleBrand()
    {
        return $this->belongsTo(VehicleBrand::class, 'vehicle_brand_id');
    }

    public function vehicles()
    {
        return $this->hasMany(Vehicle::class);
    }

    public function getUrlPath()
    {
        return Storage::url($this->path);
    }
}
