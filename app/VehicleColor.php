<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleColor extends Model
{
    protected $fillable = [
        'color'
    ];

    public function vehicles()
    {
        return $this->hasMany(Vehicle::class);
    }
}
