<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserForVehicleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return view('vehicleUsers.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vehicleUsers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'status' => 'required|string|in:inactive,active',
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        if ($request->file('img')) {
            $img = $request->file('img');
            $img->store('public/images');
            $url = $img->hashName('/images');
        }

        User::create([
            'name' => ucwords(request('name')),
            'email' => request('email'),
            'status' => request('status') == 'active',
            'password' => Hash::make(request('password')),
            'img' => $url
        ]);

        return redirect()->route('vehicleUsers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('vehicleUsers.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('vehicleUsers.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'status' => 'required|string|in:inactive,active',
        ]);


        if ($request->file('img')) {
            $img = $request->file('img');
            $img->store('public/images');
            $url = $img->hashName('/images');
        }

        $user = User::find($id);
        $user->name = ucwords(request('name'));
        $user->status = request('status') == 'active';
        $user->img = $url;
        $user->save();

        return redirect()->route('vehicleUsers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            User::find($id)->delete();
        } catch (QueryException $e) {
            $message = "This model is using by another table. You can't delete this model.";
            return back()->with('error', $message);
        }
        return redirect()->route('vehicleUsers.index');
    }
}
