<?php

namespace App\Http\Controllers;

use App\Vehicle;
use App\VehicleBrand;
use App\VehicleModel;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class VehicleModelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicleModels = VehicleModel::all();

        return view('vehicleModels.index', compact('vehicleModels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vehicles = VehicleBrand::all();
        return view('vehicleModels.create', compact('vehicles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $request->validate([
            'model' => 'required|unique:vehicle_models,model',
            'model_year' => 'required|Integer|date_format:Y',
            'vehicle_brand_id' => 'required|integer|exists:vehicle_brands,id',
            'img' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $img = $request->file('img');
        $img->store('public/images');
        $url = $img->hashName('/images');


        VehicleModel::create([
            'model' => strtoupper(request('model')),
            'model_year' => request('model_year'),
            'vehicle_brand_id' => request('vehicle_brand_id'),
            'img' => $url
        ]);



        return redirect()->route('vehicleModels.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vehicleModel = VehicleModel::find($id);
        return view('vehicleModels.show', compact('vehicleModel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vehicles = VehicleBrand::all();
        $vehicleModel = VehicleModel::find($id);
        return view('vehicleModels.edit', compact('vehicleModel','vehicles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'model' => 'required',
            'model_year' => 'required|Integer|date_format:Y',
            'vehicle_brand_id' => 'required|integer|exists:vehicle_brands,id',
        ]);

        if ($request->file('img')) {
            $img = $request->file('img');
            $img->store('public/images');
            $url = $img->hashName('/images');
        }

        $vehicle = VehicleModel::find($id);
        $vehicle->model = strtoupper(request('model'));
        $vehicle->model_year = request('model_year');
        $vehicle->vehicle_brand_id = request('vehicle_brand_id');
        $vehicle->img = $url;
        $vehicle->save();




        return redirect()->route('vehicleModels.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            VehicleModel::find($id)->delete();
        } catch (QueryException $e) {
            $message = "This model is using by another table. You can't delete this model.";
            return back()->with('error', $message);
        }

        return redirect()->route('vehicleModels.index');
    }
}
