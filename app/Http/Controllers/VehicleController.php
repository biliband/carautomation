<?php

namespace App\Http\Controllers;

use App\User;
use App\Vehicle;
use App\VehicleBrand;
use App\VehicleColor;
use App\VehicleModel;
use App\VehicleType;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class VehicleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicles = Vehicle::all();

        return view('vehicles.index', compact('vehicles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vehicleModels = VehicleModel::all();
        $users = User::all();
        $vehicleColors = VehicleColor::all();
        $vehicleTypes = VehicleType::all();

        return view('vehicles.create', compact('vehicleModels', 'users', 'vehicleColors', 'vehicleTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validated = $request->validate([
            'plate' => ['required', 'unique:vehicles,plate', 'regex: /^(0[1-9]|[1-7][0-9]|8[0-1])\s\w{1,3}\s\d{1,4}$/'],
            'nickname' => 'required|string',
            'status' => 'required|string|in:inactive,active',
            'vehicle_model_id' => 'required|integer|exists:vehicle_models,id',
            'user_id' => 'required|integer|exists:users,id',
            'vehicle_color_id' => 'required|integer|exists:vehicle_colors,id',
            'vehicle_type_id' => 'required|integer|exists:vehicle_types,id'
        ]);


        Vehicle::create([
            'plate' => strtoupper(request('plate')),
            'nickname' => request('nickname'),
            'status' => request('status') == 'active',
            'vehicle_model_id' => request('vehicle_model_id'),
            'user_id' => request('user_id'),
            'vehicle_color_id' => request('vehicle_color_id'),
            'vehicle_type_id' => request('vehicle_type_id')
        ]);


        return redirect()->route('vehicles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vehicle = Vehicle::find($id);
        return view('vehicles.show', compact('vehicle'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vehicle = Vehicle::find($id);
        $vehicleModels = VehicleModel::all();
        $users = User::all();
        $vehicleColors = VehicleColor::all();
        $vehicleTypes = VehicleType::all();
        return view('vehicles.edit', compact('vehicle', 'vehicleModels', 'users', 'vehicleColors', 'vehicleTypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'nickname' => 'required|string',
            'status' => 'required|string|in:inactive,active',
            'vehicle_model_id' => 'required|integer|exists:vehicle_models,id',
            'user_id' => 'required|integer|exists:users,id',
            'vehicle_color_id' => 'required|integer|exists:vehicle_colors,id',
            'vehicle_type_id' => 'required|integer|exists:vehicle_types,id'
        ]);


        $vehicle = Vehicle::find($id);
        $vehicle->nickname = request('nickname');
        $vehicle->status = request('status') == 'active';
        $vehicle->vehicle_model_id = request('vehicle_model_id');
        $vehicle->user_id = request('user_id');
        $vehicle->vehicle_type_id = request('vehicle_type_id');
        $vehicle->vehicle_color_id = request('vehicle_color_id');
        $vehicle->save();


        return redirect()->route('vehicles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Vehicle::find($id)->delete();
        } catch (QueryException $e) {
            $message = "This model is using by another table. You can't delete this model.";
            return back()->with('error', $message);
        }
        return redirect()->route('vehicles.index');
    }
}
