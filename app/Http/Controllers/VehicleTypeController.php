<?php

namespace App\Http\Controllers;

use App\VehicleType;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class VehicleTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicleTypes = VehicleType::all();

        return view('vehicleTypes.index', compact('vehicleTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vehicleTypes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'type' => 'required|unique:vehicle_types,type'
        ]);


        VehicleType::create([
            'type' => strtoupper(request('type'))
        ]);
        return redirect()->route('vehicleTypes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vehicleType = VehicleType::find($id);
        return view('vehicleTypes.show', compact('vehicleType'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vehicleType = VehicleType::find($id);
        return view('vehicleTypes.edit', compact('vehicleType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'type' => 'required'
        ]);

        $vehicleModel = VehicleType::find($id);
        $vehicleModel -> type  = strtoupper(request('type'));
        $vehicleModel->save();

        return redirect()->route('vehicleTypes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            VehicleType::find($id)->delete();
        } catch (QueryException $e) {
            $message = "This model is using by another table. You can't delete this model.";
            return back()->with('error', $message);
        }
        return redirect()->route('vehicleTypes.index');
    }
}
