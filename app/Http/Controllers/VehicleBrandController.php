<?php

namespace App\Http\Controllers;

use App\VehicleBrand;
use App\VehicleModel;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class VehicleBrandController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicleBrands = VehicleBrand::all();
        return view('vehicleBrands.index', compact('vehicleBrands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vehicleBrands.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'vehicle_brand' => 'required|unique:vehicle_brands,vehicle_brand'
        ]);

        VehicleBrand::create([
            'vehicle_brand' => strtoupper(request('vehicle_brand'))
        ]);

        return redirect()->route('vehicleBrands.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vehicleBrand = VehicleBrand::find($id);
        $vehicleModels = VehicleModel::all();
        return view('vehicleBrands.show', compact('vehicleBrand', 'vehicleModels'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vehicleBrand = VehicleBrand::find($id);
        return view('vehicleBrands.edit', compact('vehicleBrand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'vehicle_brand' => 'required|unique:vehicle_brands,vehicle_brand'
        ]);

        $vehicle = VehicleBrand::find($id);
        $vehicle->vehicle_brand = strtoupper(request('vehicle_brand'));

        $vehicle->save();


        return redirect()->route('vehicleBrands.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            VehicleBrand::find($id)->delete();
        } catch (QueryException $e) {
            $message = "This model is using by another table. You can't delete this model.";
            return back()->with('error', $message);
        }
        return redirect()->route('vehicleBrands.index', compact('text'));
    }
}
