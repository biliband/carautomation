<?php

namespace App\Http\Controllers;

use App\VehicleColor;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class VehicleColorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicleColors = VehicleColor::all();

        return view('vehicleColors.index', compact('vehicleColors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vehicleColors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'color' => 'required|unique'
        ]);

        VehicleColor::create([
            'color' => strtoupper(request('color'))
        ]);

        return redirect()->route('vehicleColors.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vehicleColor = VehicleColor::find($id);
        return view ('vehicleColors.show', compact('vehicleColor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vehicleColor = VehicleColor::find($id);
        return view('vehicleColors.edit', compact('vehicleColor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'color' => 'required|unique'
        ]);

        $vehicleColor = VehicleColor::find($id);
        $vehicleColor -> color  = strtoupper(request('color'));
        $vehicleColor->save();

        return redirect()->route('vehicleColors.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            VehicleColor::find($id)->delete();
        } catch (QueryException $e) {
            $message = "This model is using by another table. You can't delete this model.";
            return back()->with('error', $message);
        }

        return redirect()->route('vehicleColors.index');
    }
}
