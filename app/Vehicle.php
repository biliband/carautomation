<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $fillable = [
        'plate', 'nickname',
        'status', 'vehicle_model_id' ,
        'user_id', 'vehicle_color_id',
        'vehicle_type_id'
    ];



 public function vehicleModel()
 {
     return $this->belongsTo(VehicleModel::class, 'vehicle_model_id');
 }

 public function vehicleColor()
 {
     return $this->belongsTo(VehicleColor::class,'vehicle_color_id');
 }

 public function vehicleType()
 {
     return $this->belongsTo(VehicleType::class, 'vehicle_type_id');
 }

 public function user()
 {
     return $this->belongsTo(User::class,'user_id');
 }

}
