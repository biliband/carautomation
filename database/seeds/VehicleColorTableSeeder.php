<?php

use Illuminate\Database\Seeder;

class VehicleColorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\VehicleColor::class, 5)->create();
    }
}
