<?php

use App\VehicleBrand;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /*$this->call(VehicleBrandTableSeeder::class);*/
        $this->call(VehicleColorTableSeeder::class);
        /*$this->call(VehicleTypeTableSeeder::class);*/
        $this->call(UserTableSeeder::class);

    }
}
