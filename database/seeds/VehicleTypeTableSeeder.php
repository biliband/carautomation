<?php

use Illuminate\Database\Seeder;

class VehicleTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\VehicleType::create([
           'type' => 'Limo'
        ]);

        \App\VehicleType::create([
            'type' => 'Bus'
        ]);

        \App\VehicleType::create([
            'type' => 'Taxi'
        ]);

        \App\VehicleType::create([
            'type' => 'Chopper'
        ]);
    }
}
