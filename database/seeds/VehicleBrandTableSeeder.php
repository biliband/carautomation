<?php

use Illuminate\Database\Seeder;

class VehicleBrandTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\VehicleBrand::class, 10)
            ->create()
            ->each(function ($brand) {
                $brand->models()->saveMany(factory(App\VehicleModel::class, random_int(1, 10))->make());
            });

    }
}
