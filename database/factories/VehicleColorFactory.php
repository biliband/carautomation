<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\VehicleColor;
use Faker\Generator as Faker;

$factory->define(VehicleColor::class, function (Faker $faker) {
    return [
        'color' => $faker->colorName
    ];
});
