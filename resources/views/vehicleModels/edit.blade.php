@extends('layouts.app')



    <div class="container">
        @section('content')
            @if($errors->count())
                @foreach ($errors->all() as $error)
                    <div class="text-danger text-right">{{ $error }}</div>
                @endforeach
            @endif
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit Vehicle Model Name</div>

                    <div class="card-body">
                        <div class="container">
                            <form action="/vehicleModels/{{$vehicleModel->id}}" method="post"
                                  enctype="multipart/form-data">
                                @method('PATCH')
                                @csrf

                                <div class="form-group">
                                    <label for="model">Model Name</label>
                                    <input type="text" class="form-control @error('model') is-invalid @enderror"
                                           id="model"
                                           name="model" value="{{$vehicleModel->model}}">
                                    @error('model')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="year">Model Year</label>
                                    <input type="text" class="form-control @error('year') is-invalid @enderror"
                                           id="year"
                                           name="model_year" value="{{$vehicleModel->model_year}}">
                                    @error('year')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="img">image file</label>
                                    <input type="file" class="form-control @error('model_year') is-invalid @enderror"
                                           id="img"
                                           name="img"
                                           >
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Select Brand</label>
                                    <select
                                        class="my-select2 form-control @error('vehicle_brand_id') is-invalid @enderror"
                                        id="exampleFormControlSelect1" name="vehicle_brand_id">
                                        <option selected>{{$vehicleModel->vehicleBrand->vehicle_brand}}</option>
                                        @foreach ($vehicles as $vehicle)
                                            <option value="{{$vehicle->id}}">{{$vehicle->vehicle_brand}}</option>
                                        @endforeach
                                    </select>
                                </div>



                                <button type="submit" class="btn btn-primary">Update</button>

                                <a class="btn btn-primary" href="http://carautomation.test/vehicleModels/"
                                   role="button">Cancel
                                </a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
