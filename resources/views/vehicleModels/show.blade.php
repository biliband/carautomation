@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="/vehicleModels/{{$vehicleModel->id}}/edit" method="get">
            @csrf
            <div class="card" style="width: 18rem;">
                <img src="{{$vehicleModel->img}}" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">
                        Model: {{$vehicleModel->model}} {{$vehicleModel->vehicleBrand->vehicle_brand}}</h5>
                    <p class="card-text">This vehicles are using for specific clients, our transporter Jason Statham
                        drives safe</p>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">This car models year is {{$vehicleModel->model_year}}</li>
                </ul>

                <button type="submit" class="btn btn-primary">Edit</button>


                <form action="/vehicleModels/{{$vehicleModel->id}}" method="post">
                @method('DELETE')
                @csrf
                <!-- Button trigger modal -->
                    <button type="button" class="btn btn-dark" data-toggle="modal"
                            data-target="#deleteModal">
                        Delete
                    </button>
                    <!-- Modal -->
                    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog"
                         aria-labelledby="eminMi" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="eminMi">Are You Sure?</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                    </button>
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </form>
    </div>



@endsection
