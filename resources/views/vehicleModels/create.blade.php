@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Add New Vehicle Model</div>



                    <div class="card-body">
                        <form action="{{route('vehicleModels.store')}}" method="post" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group">
                                <label for="model">Vehicle Model</label>
                                <input type="text" style="text-transform: uppercase;"  class="form-control @error('model') is-invalid @enderror" id="model"
                                       name="model" value="{{old('model')}}" placeholder="example: RX8, TT, A100">
                                @error('model')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="modelYear">Model Year</label>
                                <input type="text" style="text-transform: uppercase;"  class="form-control @error('model_year') is-invalid @enderror" id="modelYear"
                                       name="model_year" value="{{old('model_year')}}" placeholder="example: 1996, 2005, 2019">
                                @error('model_year')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="img">image file</label>
                                <input type="file" class="form-control"
                                       id="img"
                                       name="img">
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Select Brand</label>
                                <select class="my-select2 form-control @error('vehicle_brand_id') is-invalid @enderror" id="exampleFormControlSelect1" name="vehicle_brand_id">
                                    <option selected>Choose One</option>
                                    @foreach ($vehicles as $vehicle)
                                        <option value="{{$vehicle->id}}">{{$vehicle->vehicle_brand}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <button type="submit" class="btn btn-primary">Add</button>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
