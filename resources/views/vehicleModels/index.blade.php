@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card-columns">
            @foreach($vehicleModels as $model)
                <div class="card">
                    @isset($model->img)
                        <img src="{{$model->img}}" class="card-img-top" alt="...">
                    @endisset
                    <div class="card-body">
                        <h5 class="card-title">
                            <a href="/vehicleModels/{{$model->id}}">{{$model->model}}</a>
                        </h5>
                        <p class="card-text">{{$model->vehicleBrand->vehicle_brand}} {{$model->model}} is available now. It can be used by chosen drivers.</p>
                    </div>
                    <div class="card-footer">
                        <small class="text-muted">Model Year: {{$model->model_year}}</small>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
