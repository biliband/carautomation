@extends('layouts.app')

@section('content')
    <div class="container features">
        <div class="row justify-content-around">

            <div class="card bg-light mb-3 col-lg-4 col-md-4 col-sm-12" style="max-width: 18rem;">
                <h3 class="card-header" style="text-align: center">{{$vehicle->plate}}</h3>
                <img src="{{$vehicle->vehicleModel->img}}" class="card-img-top">
                <div class="card-body">
                    <table class="table table-bordered" style="text-align: center">
                        <thead>
                        <tr>
                            <th scope="col">Model</th>
                            <th scope="col">Alias</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{$vehicle->vehicleModel->model}}</td>
                            <td>{{$vehicle->nickname}}</td>
                        </tr>
                        </tbody>
                    </table>
                    <form action="/vehicles/{{$vehicle->id}}/edit" method="get">
                        @csrf
                        <p class="card-text" style="text-align: center">{{$vehicle->status ? 'Active' : 'Inactive'}}</p>
                </div>
                <button type="submit" class="btn btn-primary">Edit</button>
                <form action="/vehicles/{{$vehicle->id}}" method="post">
                @method('DELETE')
                @csrf
                <!-- Button trigger modal -->
                    <button type="button" class="btn btn-dark" data-toggle="modal"
                            data-target="#deleteModal">
                        Delete
                    </button>
                    <!-- Modal -->
                    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog"
                         aria-labelledby="eminMi" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="eminMi">Are You Sure?</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                    </button>
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                </form>
            </div>
            <div class="card col-lg-3 col-md-4 col-sm-12">
            <div class="container">
                <h5 class="feature-title" style="padding: 6px">Driver: <a href="/vehicleUsers/{{$vehicle->user->id}}">{{$vehicle->user->name}}</a></h5>

                <img src="{{$vehicle->user->img}}" class="card-img-top" style="align-content: center">

                <p style="text-align: center">
                    {{$vehicle->user->status ? 'Active' : 'Inactive'}}
                </p>

                <p style="padding: 6px">
                    Mail => {{$vehicle->user->email}}
                </p>
            </div>

        </div>
    </div>



@endsection


