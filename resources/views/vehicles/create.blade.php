@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Add New Vehicle</div>


                    <div class="card-body">
                        <form action="{{route('vehicles.store')}}" method="post">
                            @csrf


                            <div class="form-group">
                                <label for="plate">License Plate</label>
                                <input type="text" style="text-transform: uppercase;"
                                       value="{{old('plate')}}"
                                       class="form-control @error('plate') is-invalid @enderror" id="plate"
                                       name="plate" placeholder="example: 35 GTR 8978">
                                @error('plate')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="nickname">Nickname of Car</label>
                                <input type="text"
                                       value="{{old('nickname')}}"
                                       class="form-control @error('nickname') is-invalid @enderror"
                                       id="nickname" name="nickname"
                                       placeholder="example: North-West Boulevard Bull">
                                @error('nickname')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <fieldset class="form-group">
                                <div class="row">
                                    <legend class="col-form-label col-sm-2 pt-0 text-nowrap">Vehicle Status</legend>
                                    <div class="col-sm-10">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="status" id="gridRadios1"
                                                   value="active" checked>
                                            <label class="form-check-label" for="gridRadios1">
                                                Active
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="status" id="gridRadios2"
                                                   value="inactive">
                                            <label class="form-check-label" for="gridRadios2">
                                                Inactive
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <div class="form-group">
                                <label for="model">Select Model</label>
                                <select class="my-select2 form-control @error('vehicle_model_id') is-invalid @enderror"
                                        id="model" name="vehicle_model_id">
                                    <option selected>Choose One</option>
                                    @foreach ($vehicleModels as $model)
                                        <option value="{{$model->id}}">{{$model->model}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="user">Select User</label>
                                <select class="my-select2 form-control @error('user_id') is-invalid @enderror"
                                        id="user" name="user_id">
                                    <option selected>Choose One</option>
                                    @foreach ($users as $user)
                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="color">Select Vehicle Color</label>
                                <select class="my-select2 form-control @error('vehicle_color_id') is-invalid @enderror"
                                        id="color" name="vehicle_color_id">
                                    <option selected>Choose One</option>
                                    @foreach ($vehicleColors as $color)
                                        <option value="{{$color->id}}">{{$color->color}}</option>
                                    @endforeach
                                </select>
                            </div>


                                <div class="form-group">
                                    <label for="type">Select Vehicle Type</label>
                                    <select class="my-select2 form-control @error('vehicle_type_id') is-invalid @enderror"
                                            id="type" name="vehicle_type_id">
                                        <option selected>Choose One</option>
                                        @foreach ($vehicleTypes as $type)
                                            <option value="{{$type->id}}">{{$type->type}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <button type="submit" class="btn btn-primary">Add</button>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

