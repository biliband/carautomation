@extends('layouts.app')

@section('content')
    <style>
        .green {
            color: green;
            font-weight: bold;
        }

        .red {
            color: red;
            font-weight: bold;
        }
    </style>

    <div class="container">
        <div class="card">
            <table id="myTable" class="display dataTable" style="width: 100%" role="grid">
                <thead>
                <tr>
                    <th>#</th>
                    <th>License Plate</th>
                    <th>Nickname</th>
                    <th>Status</th>
                    <th>Model</th>
                    <th>User</th>
                    <th>Vehicle Type</th>
                    <th>Vehicle Color</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($vehicles as $vehicle)
                    <tr>
                        <th scope="row"><a href="/vehicles/{{$vehicle->id}}">{{$vehicle->id}}</a></th>
                        <td><a href="/vehicles/{{$vehicle->id}}">{{$vehicle->plate}}</a></td>
                        <td>{{$vehicle->nickname}}</td>
                        <td class="{{$vehicle->status == 'Active' ? 'red' : 'green'}}">{{$vehicle->status ? 'Active' : 'Inactive'}}</td>
                        <td
                            data-container="body"
                            data-toggle="popover"
                            data-placement="top"
                            data-trigger="hover"
                            data-content="{{$vehicle->vehicleModel->model_year}}">
                            {{$vehicle->vehicleModel->model}}
                        </td>
                        <td>{{$vehicle->user->name}}</td>
                        <td>{{$vehicle->vehicleType->type}}</td>
                        <td>{{$vehicle->vehicleColor->color}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
