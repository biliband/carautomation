@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit Vehicle</div>
                    <div class="container">
                            <label for="plate">License Plate</label>
                            <input type="text" class="form-control @error('plate') is-invalid @enderror" id="plate"
                                   name="plate" value="{{$vehicle->plate}}" disabled>
                            @error('plate')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                    </div>
                    <div class="card-body">
                        <form action="/vehicles/{{$vehicle->id}}" method="post">
                            @method('PATCH')
                            @csrf
                            <div class="form-group">
                                <label for="nickname">Nickname of Car</label>
                                <input type="text" class="form-control" id="nickname" name="nickname"
                                       value="{{$vehicle->nickname}}">
                            </div>
                            <fieldset class="form-group">
                                <div class="row">
                                    <legend class="col-form-label col-sm-2 pt-0 text-nowrap">Vehicle Status</legend>
                                    <div class="col-sm-10">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="status" id="gridRadios1"
                                                   value="active" checked>
                                            <label class="form-check-label" for="gridRadios1">
                                                Active
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="status" id="gridRadios2"
                                                   value="inactive">
                                            <label class="form-check-label" for="gridRadios2">
                                                Inactive
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Select Model</label>
                                <select class="form-control @error('vehicle_model_id') is-invalid @enderror" id="exampleFormControlSelect1" name="vehicle_model_id">
                                    @foreach ($vehicleModels as $model)
                                        <option value="{{$model->id}}">{{$model->model}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Select User</label>
                                <select class="form-control @error('user_id') is-invalid @enderror" id="exampleFormControlSelect1" name="user_id">
                                    @foreach ($users as $user)
                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Select Color</label>
                                <select class="form-control @error('vehicle_color_id') is-invalid @enderror" id="exampleFormControlSelect1" name="vehicle_color_id">
                                    @foreach ($vehicleColors as $color)
                                        <option value="{{$color->id}}">{{$color->color}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Select Vehicle Type</label>
                                <select class="form-control @error('vehicle_type_id') is-invalid @enderror" id="exampleFormControlSelect1" name="vehicle_type_id">
                                    @foreach ($vehicleTypes as $type)
                                        <option value="{{$type->id}}">{{$type->type}}</option>
                                    @endforeach
                                </select>
                            </div>


                            <button type="submit" class="btn btn-primary">Update</button>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
