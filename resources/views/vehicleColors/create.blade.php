@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Add New Vehicle Color</div>



                    <div class="card-body">
                        <form action="{{route('vehicleColors.store')}}" method="post">
                            @csrf

                            <div class="form-group">
                                <label for="color">Vehicle Color</label>
                                <input type="text" style="text-transform: uppercase;"  class="form-control @error('color') is-invalid @enderror" id="color"
                                       name="color" value="{{old('color')}}" placeholder="example: Blue, Red, Yellow">
                                @error('color')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <button type="submit" class="btn btn-primary">Add</button>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
