@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <table id="myTable" class="display dataTable" style="width: 100%" role="grid">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Vehicle Colors</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($vehicleColors as $color)
                    <tr>
                        <th scope="row"><a href="/vehicleColors/{{$color->id}}">{{$color->id}}</a></th>
                        <td><a href="/vehicleColors/{{$color->id}}">{{$color->color}}</a></td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
