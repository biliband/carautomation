@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Add New Vehicle Brand</div>



                    <div class="card-body">
                        <form action="{{route('vehicleBrands.store')}}" method="post">
                            @csrf

                            <div class="form-group">
                                <label for="brand">Vehicle Brand</label>
                                <input type="text" style="text-transform: uppercase;"  class="form-control @error('vehicle_brand') is-invalid @enderror" id="brand"
                                       name="vehicle_brand" value="{{old('vehicle_brand')}}" placeholder="example: BMW, AUDI, MERCEDES">
                                @error('vehicle_brand')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <button type="submit" class="btn btn-primary">Add</button>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
