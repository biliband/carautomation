@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit Vehicle Brand Name</div>

                    @if($errors->count())
                        @foreach ($errors->all() as $error)
                            <div class="text-danger text-right">{{ $error }}</div>
                        @endforeach
                    @endif

                </div>
                <div class="card-body">
                    <form action="/vehicleBrands/{{$vehicleBrand->id}}" method="post">
                        @method('PATCH')
                        @csrf

                        <div class="container">
                            <div class="form-group">
                                <label for="brand">Brand Name</label>
                                <input type="text" class="form-control @error('brand') is-invalid @enderror" id="brand"
                                       name="vehicle_brand" value="{{$vehicleBrand->vehicle_brand}}">
                                @error('brand')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                            <button type="submit" class="btn btn-primary">Update</button>
                            <a class="btn btn-primary" href="http://carautomation.test/vehicleBrands/" role="button">Cancel</a>
                    </form>

                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
