@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card bg-light mb-3" style="max-width: 18rem;">
            <h3 class="card-header" style="text-align: center">{{$vehicleBrand->vehicle_brand}}</h3>
            <div class="card-body">
                <form action="/vehicleBrands/{{$vehicleBrand->id}}/edit" method="get">
                @csrf
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
            <form action="/vehicleBrands/{{$vehicleBrand->id}}" method="post">
            @method('DELETE')
            @csrf
            <!-- Button trigger modal -->
                <button type="button" class="btn btn-dark" data-toggle="modal"
                        data-target="#deleteModal">
                    Delete
                </button>
                <!-- Modal -->
                <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog"
                     aria-labelledby="eminMi" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="eminMi">Are You Sure?</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                </button>
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            </form>
        </div>
    </div>

@endsection


