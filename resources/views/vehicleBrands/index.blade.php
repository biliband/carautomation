@extends('layouts.app')

@section('content')
    @isset($text)
        <p>{{$text}}</p>
    @endisset
    <div class="container">
        <div class="card">
            <table id="myTable" class="display dataTable" style="width: 100%" role="grid">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Vehicle Brands</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($vehicleBrands as $brand)
                    <tr>
                        <th scope="row"><a href="/vehicleBrands/{{$brand->id}}">{{$brand->id}}</a></th>
                        <td><a href="/vehicleBrands/{{$brand->id}}">{{$brand->vehicle_brand}}</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
