@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Add New Vehicle Type</div>



                    <div class="card-body">
                        <form action="{{route('vehicleTypes.store')}}" method="post">
                            @csrf

                            <div class="form-group">
                                <label for="type">Vehicle Type</label>
                                <input type="text" style="text-transform: uppercase;"  class="form-control @error('type') is-invalid @enderror" id="type"
                                       name="type" placeholder="example: Bus, Car, Taxi">
                                @error('type')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <button type="submit" class="btn btn-primary">Add</button>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
