@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit Vehicle Type</div>

                    @if($errors->count())
                        @foreach ($errors->all() as $error)
                            <div class="text-danger text-right">{{ $error }}</div>
                        @endforeach
                    @endif

                </div>
                <div class="card-body">
                    <form action="/vehicleTypes/{{$vehicleType->id}}" method="post">
                        @method('PATCH')
                        @csrf

                        <div class="container">
                            <div class="form-group">
                                <label for="type">Type Name</label>
                                <input type="text" class="form-control @error('type') is-invalid @enderror" id="type"
                                       name="type" value="{{$vehicleType->type}}">
                                @error('type')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                            <button type="submit" class="btn btn-primary">Update</button>
                            <a class="btn btn-primary" href="http://carautomation.test/vehicleTypes/" role="button">Cancel</a>
                    </form>

                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
