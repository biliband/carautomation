@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <table id="myTable" class="display dataTable" style="width: 100%" role="grid">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Vehicle Types</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($vehicleTypes as $type)
                    <tr>
                        <th scope="row"><a href="/vehicleTypes/{{$type->id}}">{{$type->id}}</a></th>
                        <td><a href="/vehicleTypes/{{$type->id}}">{{$type->type}}</a></td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
