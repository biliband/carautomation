@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit User</div>
                    <div class="container">
                        <label for="user">User Name</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" id="name"
                               name="name" value="{{$user->name}}" disabled>
                        @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="card-body">
                        <form action="/vehicleUsers/{{$user->id}}" method="post" enctype="multipart/form-data">
                            @method('PATCH')
                            @csrf
                            <div class="form-group">
                                <label for="newName">New User Name</label>
                                <input type="text" class="form-control" id="newName" name="name"
                                       value="{{$user->name}}">
                            </div>

                            <fieldset class="form-group">
                                <div class="row">
                                    <legend class="col-form-label col-sm-2 pt-0 text-nowrap">User Status</legend>
                                    <div class="col-sm-10">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="status" id="gridRadios1"
                                                   value="active" checked>
                                            <label class="form-check-label" for="gridRadios1">
                                                Active
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="status" id="gridRadios2"
                                                   value="inactive">
                                            <label class="form-check-label" for="gridRadios2">
                                                Inactive
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <div class="form-group">
                                <label for="img">image file</label>
                                <input type="file" class="form-control @error('img') is-invalid @enderror" id="img"
                                       name="img">
                            </div>

                            <button type="submit" class="btn btn-primary">Update</button>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
