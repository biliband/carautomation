@extends('layouts.app')

@section('content')
    <style>
        .green {
            color: green;
            font-weight: bold;
        }

        .red {
            color: red;
            font-weight: bold;
        }
    </style>
    <div class="container">
        <div class="card">
            <table id="myTable" class="display dataTable" style="width: 100%" role="grid">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">User</th>
                    <th scope="col">Email</th>
                    <th scope="col">Status</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($users as $user)
                    <tr>
                        <th scope="row"><a href="/vehicleUsers/{{$user->id}}">{{$user->id}}</a></th>
                        <td><a href="/vehicleUsers/{{$user->id}}">{{$user->name}}</a></td>
                        <td>{{$user->email}}</td>
                        <td class="{{$user->status == 'Active' ? 'red' : 'green'}}">{{$user->status ? 'Active' : 'Inactive'}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
